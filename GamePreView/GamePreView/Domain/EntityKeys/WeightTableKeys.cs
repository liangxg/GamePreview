﻿namespace GamePreView.Domain.EntityKeys
{
    public class WeightTableKeys
    {        
        public int GameId { get; set; }        
        public int StateId { get; set; }
        public string WeightTableName { get; set; }
        public string Variation { get; set; }
        public int Bet { get; set; }
        public int Line { get; set; }
        public int Denom { get; set; }       
        public string ActionValue { get; set; }
    }
}
