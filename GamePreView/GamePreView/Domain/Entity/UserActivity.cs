﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace GamePreView.Domain.Entity
{
    public class UserActivity : EntityBase
    {
        [Required]
        public int UserActivityId { get; set; }
        [Required]
        public int UserId { get; set; }
        [Required]
        public int Action { get; set; }
        public DateTime ActionTs { get; set; }
        [Required]
        public string TableName { get; set; }
        public User User { get; set; }
    }
}
