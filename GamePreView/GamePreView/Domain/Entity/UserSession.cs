﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace GamePreView.Domain.Entity
{
    public class UserSession : EntityBase
    {
        [Required]
        public int UserSessionId { get; set; }
        [Required]
        public int UserId { get; set; }
        [Required]
        public string HashKey { get; set; }
        [Required]
        public string HashValue { get; set; }
        public int Status   { get; set; }
        public DateTime SessionStart    { get; set; }
        public DateTime SessionEnd { get; set; }
        public string Memo { get; set; }
        public User User { get; set; }

    }
}
