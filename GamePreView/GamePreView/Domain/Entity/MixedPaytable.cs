﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace GamePreView.Domain.Entity
{
    public class MixedPaytable : EntityBase
    {
        [Required]
        public int MixedPaytableId { get; set; }
        [Required]
        public int GameId { get; set; }
        [Required]
        public int StateId { get; set; }
        public int CreateUserId { get; set; }
        public int UpdateUserId { get; set; }
        [Required]
        public int SymbolId { get; set; }
        public string SymbolName { get; set; }
        public int Bet { get; set; }
        public int Line { get; set; }
        public int Denom { get; set; }
        public string Variation { get; set; }
        public string ReelStrips { get; set; }
        public int K_1 { get; set; }
        public int K_2 { get; set; }
        public int K_3 { get; set; }
        public int K_4 { get; set; }
        public int K_5 { get; set; }
        public int K_6 { get; set; }
        public int K_7 { get; set; }
        public int K_8 { get; set; }
        public string Memo { get; set; }
    }
}
