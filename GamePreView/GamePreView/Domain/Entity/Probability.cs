﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace GamePreView.Domain.Entity
{
    public class Probability : EntityBase
    {
        [Required]
        public int GameId { get; set; }
        [Required]
        public int StateId { get; set; }
        [Required]
        public string ProbabilityName { get; set; }
        public int CreateUserId { get; set; }
        public int UpdateUserId { get; set; }
        public int Bet { get; set; }
        public int Line { get; set; }
        public int Denom { get; set; }
        public string Variation { get; set; }
        [Required]
        public double Total { get; set; }
        [Required]
        public double Shot { get; set; }
        public int ReferenceId { get; set; }
        public string Memo { get; set; }

        public GameState GameState { get; set; }

    }
}
