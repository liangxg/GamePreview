﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace GamePreView.Domain.Entity
{
    public class ReelStrip : EntityBase
    {
        [Required]
        public int GameId { get; set; }
        [Required]
        public int StateId { get; set; }
        public int CreateUserId { get; set; }
        public int UpdateUserId { get; set; }
        [Required]
        public int ReelNo { get; set; }
        [Required]
        public int SetNo { get; set; }
        public int Bet { get; set; }
        public int Line { get; set; }
        public int Denom { get; set; }
        public string Variation { get; set; }
        [Required]
        public string ReelStripValue { get; set; }
        public string Memo { get; set; }

        public GameState GameState { get; set; }
    }
}
