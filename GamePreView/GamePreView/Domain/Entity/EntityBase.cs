﻿using System;

namespace GamePreView.Domain.Entity
{
    public class EntityBase
    {
        public int EntityId { get; set; }       
        public DateTime? CreatedDate { get; set; }      
        public DateTime? LastModifiedDate { get; set; }
    }
}
