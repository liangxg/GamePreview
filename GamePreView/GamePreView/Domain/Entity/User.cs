﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace GamePreView.Domain.Entity
{
    public class User : EntityBase
    {
        [Required]
        public int UserId { get; set; }
        [Required]
        public string FirstName { get; set; }
        public string LastName { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public string Role { get; set; }
        [Required]
        public int  PhoneNo { get; set; }
        public int  MobileNo { get; set; }
        [Required]
        public string   Email { get; set; }
        public int  Status { get; set; }
        public DateTime UpdateTs { get; set; }
        public int SessionID { get; set; }

        public List<UserGame> Games { get; set; }
        public List<UserSession> Sessions { get; set; }
        public List<UserActivity> Activitys { get; set; }
        public List<CurrentWork> Works { get; set; }

    }

}
