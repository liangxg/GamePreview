﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace GamePreView.Domain.Entity
{
    public class Game : EntityBase
    {
        [Required]
        public int GameId { get; set; }
        [Required]
        public string GameName { get; set; }
        [Required]
        public string BrandId { get; set; }
        [Required]
        public int Status { get; set; }
        public string Designer { get; set; }
        public string Jurisdiction { get; set; }
        public DateTime CreateTs { get; set; }
        public DateTime SubmitTs { get; set; }
        public double TotalRTP  { get; set; }
        public List<UserGame> Users { get; set; }

        public List<GamePerformance> GamePerformances { get; set; }
        public List<GameState> GameStates { get; set; }

    }
}
