﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace GamePreView.Domain.Entity
{
    public class GameState : EntityBase
    {
        [Required]
        public int GameId { get; set; }
        [Required]
        public int StateId { get; set; }
        [Required]
        public string StateName { get; set; }
        [Required]
        public int PreStateId { get; set; }
        [Required]
        public string PreStateName { get; set; }
        public double TheoriticRTP { get; set; }
        [Required]
        public string TriggerSymbol { get; set; }
        [Required]
        public int TriggerKind { get; set; }
        [Required]
        public int FreeSpinNo { get; set; }
        [Required]
        public string IsRetrigger { get; set; }
        public string RetriggerSymbol { get; set; }
        public string RetriggerKind { get; set; }
        public int RetriggerSpin { get; set; }

        public Game Game { get; set; }
        public List<WeightTable> WeightTables { get; set; }
        public List<Probability> Probabilities { get; set; }
        public List<ReelStrip> ReelStrips { get; set;}
        public List<Paytable> Paytables { get; set; }
        public List<MixedPaytable> MixedPaytables { get; set; }
    }
}
