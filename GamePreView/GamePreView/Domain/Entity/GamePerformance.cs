﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace GamePreView.Domain.Entity
{
    public class GamePerformance : EntityBase
    {
        [Required]
        public int PerformanceId { get; set; }
        [Required]
        public int GameId { get; set; }
        [Required]
        public string GameName { get; set; }
        [Required]
        public int BrandId { get; set; }
        [Required]
        public int Status { get; set; }
        public string Jurisdiction { get; set; }
        public DateTime CreateTs { get; set; }
        public double TotalRTP { get; set; }
        public int ADT { get; set; }
        public double AvgToRatio { get; set; }
        public int NumOfRecord  { get; set; }

        public Game Game { get; set; }
    }
}
