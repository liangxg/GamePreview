﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace GamePreView.Domain.Entity
{
    public class UserGame : EntityBase
    {
        [Required]
        public int UserId { get; set; }
        [Required]
        public int GameId { get; set; }
        [Required]
        public int Status { get; set; }
        public string Role { get; set; }
        public DateTime CreateTs { get; set; }
        public DateTime UpdateTs { get; set; }

        public User User { get; set; }
        public Game Game { get; set; }
    }
}
