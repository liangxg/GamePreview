﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace GamePreView.Domain.Entity
{
    public class CurrentWork : EntityBase
    {
        [Required]
        public int UserId { get; set; }
        [Required]
        public int GameId { get; set; }
        [Required]
        public int SessionId { get; set; }
        [Required] 
        public int Status { get; set; }
        public DateTime CreateTs { get; set; }

    }
}
