﻿using GamePreView.Domain.Entity;

namespace GamePreView.Domain.Extensions
{
    public static class WeightTableExtensions
    {
        public static bool KeyEquals(this WeightTable orig, WeightTable dest)
        {
            if( orig == null || dest == null)
            {
                return false;
            }

            if( orig == dest)
            {
                return true;
            }

           
            if( orig.GameId == dest.GameId &&
                orig.StateId == dest.StateId &&
                orig.WeightTableName == dest.WeightTableName &&
                orig.Variation == dest.Variation &&
                orig.Bet == dest.Bet &&
                orig.Line == dest.Line &&
                orig.Denom == dest.Denom &&
                orig.ActionValue == dest.ActionValue 
               )
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        public static bool IsInSameTables(this WeightTable orig, WeightTable dest)
        {
            if (orig == null || dest == null)
            {
                return false;
            }

            if (orig == dest)
            {
                return true;
            }            

            if (orig.GameId == dest.GameId &&
                orig.StateId == dest.StateId &&
                orig.WeightTableName == dest.WeightTableName &&
                orig.Variation == dest.Variation &&
                orig.Bet == dest.Bet &&
                orig.Line == dest.Line &&
                orig.Denom == dest.Denom               
               )
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
