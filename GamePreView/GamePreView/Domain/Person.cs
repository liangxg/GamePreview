﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GamePreView.Domain
{
    public class Person
    {
        private string _name;
        public string Name
        {
            get => _name;
            set => _name = value;
        }

        public uint Age
        { get; set; }

        public int Id
        { get; set; }
            
    }


    
}
