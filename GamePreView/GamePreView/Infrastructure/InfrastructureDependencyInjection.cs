﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using GamePreView.Infrastructure.Persistence;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using GamePreView.Infrastructure.Repository;
using GamePreView.Infrastructure.Repository.Interface;

namespace GamePreView.Infrastructure
{
    public static class InfrastructureDependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                    options.UseSqlServer(
                        configuration.GetConnectionString("DefaultConnection"),
                        b => b.MigrationsAssembly(typeof(ApplicationDbContext).Assembly.FullName)));

                //If you want to change the relational database model,
                //you need to delete the migrations folder and recreate what you want to create with relational database model integration like Postgres, MySql.
                //services.AddDbContext<ApplicationDbContext>(options =>
                //    options.UseNpgsql(
                //        configuration.GetConnectionString("DefaultConnection_Postgres")));
            
            services.AddTransient<IPersonRepoistory, PersonRepoistory>();
            services.AddTransient<IWeightTableRepository, WeightTableRepository>();

            return services;
        }
    }   
}
