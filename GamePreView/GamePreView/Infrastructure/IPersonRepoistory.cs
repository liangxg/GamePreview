﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GamePreView.Domain;
using GamePreView.Application;

namespace GamePreView.Infrastructure
{
    public interface IPersonRepoistory
    {

        public  Task<List<Person>> GetAllAsync();


        public Task<Person> AddPersonAsync (Person person);


        public Task<Person> GetPersonAsync (int id);
        
    }
}

