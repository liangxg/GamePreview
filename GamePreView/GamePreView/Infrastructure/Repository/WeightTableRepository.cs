﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GamePreView.Domain.Entity;
using GamePreView.Application;
using GamePreView.Infrastructure.Repository.Interface;
using GamePreView.Infrastructure.Persistence;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using GamePreView.Domain.EntityKeys;
using GamePreView.Domain.Extensions;


namespace GamePreView.Infrastructure.Repository
{
#nullable enable
    public class WeightTableRepository : RepositoryBase<WeightTable>, IWeightTableRepository
    {
        
        public WeightTableRepository(ApplicationDbContext dbContext) : base(dbContext)
        {            


        }
        public async Task<IEnumerable<WeightTable>> GetWeightTablesByNameAsync(int gameId, string name, CancellationToken token = default)
        {
            var WeightList = await base.GetAsync(w => w.GameId == gameId &&  w.WeightTableName == name, token);
            return WeightList;
        }


        public async Task<IEnumerable<WeightTable>> GetWeightTablesByGameIdAsync(int gameId, CancellationToken token = default)
        {
            var WeightList = await base.GetAsync(w => w.GameId == gameId, token);            
           
            return WeightList;            
        }

        public async Task<WeightTable?> GetAWeightTableRecordAsync(WeightTableKeys keys, CancellationToken token = default)
        {
            object[] keylist = new object[] { keys.GameId, keys.StateId, keys.WeightTableName , keys.Variation, keys.Bet, keys.Bet, keys.Line, keys.Denom, keys.ActionValue};
            var weight = await base.GetByKeyAsync(keylist);

            return weight;
        }

        public async Task<IEnumerable<WeightTable>> GetWeightTablesBySelection(WeightTableKeys keys,  CancellationToken token = default)
        {
            var predicate = PredicateBuilder.True<WeightTable>();

            predicate = predicate.And(w => w.GameId == keys.GameId);

            if ( keys.StateId >= 0)
            {
                predicate = predicate.And(w => w.StateId == keys.StateId);
            }

            if(!string.IsNullOrEmpty(keys.WeightTableName))
            {
                predicate = predicate.And(w => w.WeightTableName == keys.WeightTableName);
            }

            if(!string.IsNullOrEmpty(keys.Variation))
            {
                predicate = predicate.And(w => w.Variation == keys.Variation);
            }

            if( keys.Bet > 0)
            {
                predicate = predicate.And(w => w.Bet == keys.Bet);
            }

            if (keys.Line > 0)
            {
                predicate = predicate.And(w => w.Line == keys.Line);
            }

            if (keys.Denom > 0)
            {
                predicate = predicate.And(w => w.Line == keys.Line);
            }


            var weightList = await _dbContext.Weights.Where(predicate).ToListAsync(token);


            return weightList;

        }

        public async Task<IOrderedEnumerable<WeightTable>> GetWeightTableRecordsAsync(int gameId, int stateId, string name, string var, int bet, int line, int denom, CancellationToken token)
        {
            var WeightList = await _dbContext.Weights.Where(w => w.GameId == gameId && w.StateId == stateId && w.WeightTableName == name)
                                    .Where(w => w.Variation == var && w.Bet == bet && w.Line == line && w.Denom == denom)
                                    .OrderBy(w => w.ActionValue)
                                    .ToListAsync(token);

            return (IOrderedEnumerable<WeightTable>)WeightList;
        }

        public async Task<ValueTuple<bool, WeightTable?>> CreateWeightTableRecordAsync(WeightTable newItem, CancellationToken cancelToken = default)
        {
            if (newItem == null)
            {
                return (false, null);
            }

            WeightTableKeys key = new WeightTableKeys { GameId = newItem.GameId, StateId = newItem.StateId, WeightTableName = newItem.WeightTableName,
                                                        Variation = newItem.Variation , Bet = newItem.Bet, Line = newItem.Line, Denom = newItem.Denom,
                                                        ActionValue = newItem.ActionValue 
                                                       };

            var record = await GetAWeightTableRecordAsync(key, cancelToken);

            if (record != null)
            {
                return (false, newItem);
            }

            try
            {
                await base.AddAsync(newItem, cancelToken);
                return (true, newItem);
            }
            catch(Exception)
            {
                return (false, null);
            }

            

            /*
            var transaction = await _dbContext.Database.BeginTransactionAsync(cancelToken);

            try
            {

                await base.AddAsync(newItem, cancelToken);

                await UpdateRangedAccumWeightAsync(newItem, cancelToken);

                await transaction.CommitAsync(cancelToken);

                return (true, newItem);
            }
            catch (Exception)
            {
                return (false, null);
            }
            */
        }

        public async Task<ValueTuple<bool, WeightTable?>> CreateMultiWeightTableRecordsAsync(List<WeightTable> newItem, CancellationToken cancelToken = default)
        {
            if (newItem == null || newItem.Count == 0)
            {
                return (false, null);
            }            

            foreach (var item in newItem)
            {
                WeightTableKeys key = new WeightTableKeys
                {
                    GameId = item.GameId,
                    StateId = item.StateId,
                    WeightTableName = item.WeightTableName,
                    Variation = item.Variation,
                    Bet = item.Bet,
                    Line = item.Line,
                    Denom = item.Denom,
                    ActionValue = item.ActionValue
                };

                var record = await GetAWeightTableRecordAsync(key, cancelToken);

                if (record != null)
                {
                    return (false, item);
                }


                await _dbContext.Weights.AddAsync(item, cancelToken);
            }

            await _dbContext.SaveChangesAsync(cancelToken);

            /*

            List<WeightTable> keysList = CheckDifferentRecordList(newItem);

            foreach(var key in keysList)
            {
                await UpdateRangedAccumWeightAsync(key, cancelToken);

            }
            */
            var last = newItem[^1];
            return (true, last);
        }


        public async Task<ValueTuple<bool, WeightTable?>> UpdateWeightTableRecordAsync(WeightTable Item, CancellationToken cancelToken = default)
        {
            if (Item == null)
            {
                return (false, null);
            }

            WeightTableKeys key = new WeightTableKeys
            {
                GameId = Item.GameId,
                StateId = Item.StateId,
                WeightTableName = Item.WeightTableName,
                Variation = Item.Variation,
                Bet = Item.Bet,
                Line = Item.Line,
                Denom = Item.Denom,
                ActionValue = Item.ActionValue
            };

            var record = await GetAWeightTableRecordAsync(key, cancelToken);

            if (record == null)
            {
                return (false, Item);
            }            


            if(record.ActionWeight != Item.ActionWeight && Item.ActionWeight >= 0)
            {
                record.ActionWeight = Item.ActionWeight;                
            }

            if (Item.ReferenceId >= 0 && record.ReferenceId != Item.ReferenceId )
            {
                record.ReferenceId = Item.ReferenceId;
            }

            if (Item.CreateUserId > 0 && Item.CreateUserId != record.CreateUserId)
            {
                record.CreateUserId = Item.CreateUserId;
            }

            if (Item.UpdateUserId > 0 && Item.UpdateUserId != record.UpdateUserId)
            {
                record.UpdateUserId = Item.UpdateUserId;
            }            
            
            if(Item.Memo != null)
            {
                record.Memo = Item.Memo;
            }

            if (Item.GameState != null)
            {
                record.GameState = Item.GameState;
            }
            

            try
            {
                await base.UpdateAsync(record, cancelToken);
                return (true, Item);
            }
            catch (Exception)
            {
                return (false, null);
            }            

        }


        public async Task<ValueTuple<bool, WeightTable?>> UpdateMultiWeightTableRecordsAsync(List<WeightTable> Items, CancellationToken cancelToken = default)
        {
            if (Items == null || Items.Count == 0)
            {
                return (false, null);
            }

            foreach (var item in Items)
            {
                WeightTableKeys key = new WeightTableKeys
                {
                    GameId = item.GameId,
                    StateId = item.StateId,
                    WeightTableName = item.WeightTableName,
                    Variation = item.Variation,
                    Bet = item.Bet,
                    Line = item.Line,
                    Denom = item.Denom,
                    ActionValue = item.ActionValue
                };

                var record = await GetAWeightTableRecordAsync(key, cancelToken);

                if (record == null)
                {
                    return (false, item);
                }

                if (record.ActionWeight != item.ActionWeight && item.ActionWeight >= 0)
                {
                    record.ActionWeight = item.ActionWeight;
                }

                if (item.ReferenceId >= 0 && record.ReferenceId != item.ReferenceId)
                {
                    record.ReferenceId = item.ReferenceId;
                }

                if (item.CreateUserId > 0 && item.CreateUserId != record.CreateUserId)
                {
                    record.CreateUserId = item.CreateUserId;
                }

                if (item.UpdateUserId > 0 && item.UpdateUserId != record.UpdateUserId)
                {
                    record.UpdateUserId = item.UpdateUserId;
                }

                if (item.Memo != null)
                {
                    record.Memo = item.Memo;
                }

                if (item.GameState != null)
                {
                    record.GameState = item.GameState;
                }

                _dbContext.Entry(record).State = EntityState.Modified;
            }

            await _dbContext.SaveChangesAsync(cancelToken);            

            var last = Items[^1];
            return (true, last);
        }


        public async Task<ValueTuple<bool, WeightTable?>> DeleteAWeightTableRecordAsync(WeightTable Item, CancellationToken cancelToken = default)
        {
            if (Item == null)
            {
                return (false, null);
            }

            WeightTableKeys key = new WeightTableKeys
            {
                GameId = Item.GameId,
                StateId = Item.StateId,
                WeightTableName = Item.WeightTableName,
                Variation = Item.Variation,
                Bet = Item.Bet,
                Line = Item.Line,
                Denom = Item.Denom,
                ActionValue = Item.ActionValue
            };

            var record = await GetAWeightTableRecordAsync(key, cancelToken);

            if (record == null)
            {
                return (true, Item);
            }


            try
            {
                await base.DeleteAsync(record, cancelToken);
                return (true, Item);
            }
            catch (Exception)
            {
                return (false, null);
            }  

        }


        public async Task<ValueTuple<bool, WeightTable?>> DeleteMultiWeightTableRecordsAsync(List<WeightTable> Items, CancellationToken cancelToken = default)
        {
            if (Items == null || Items.Count == 0)
            {
                return (false, null);
            }

            foreach (var item in Items)
            {
                WeightTableKeys key = new WeightTableKeys
                {
                    GameId = item.GameId,
                    StateId = item.StateId,
                    WeightTableName = item.WeightTableName,
                    Variation = item.Variation,
                    Bet = item.Bet,
                    Line = item.Line,
                    Denom = item.Denom,
                    ActionValue = item.ActionValue
                };

                var record = await GetAWeightTableRecordAsync(key, cancelToken);

                if (record == null)
                {
                    continue;
                }

                _dbContext.Weights.Remove(record);
                
            }

            await _dbContext.SaveChangesAsync(cancelToken);            

            var last = Items[^1];
            return (true, last);
        }

        /*
        protected async Task UpdateRangedAccumWeightAsync(WeightTable Item, CancellationToken cancelToken = default)
        {
            var WeightList = await GetWeightTableRecordsAsync(Item.GameId, Item.StateId, Item.WeightTableName, Item.Variation, Item.Bet, Item.Line, Item.Denom, cancelToken);

            double accumulateWeight = 0;

            foreach (var w in WeightList)
            {
                accumulateWeight += w.ActionWeight;
                w.AccumulatedWeight = accumulateWeight;
            }

            _dbContext.Weights.UpdateRange(WeightList);

            await _dbContext.SaveChangesAsync(cancelToken);


            return;
        }
        */

        public async Task<long> GetWeightTableRecordsNumerAsync(CancellationToken cancelToken = default)
        {
            long num = await base.GetRecordsNumAsync(cancelToken);

            return num;

        }

        protected List<WeightTable> CheckDifferentRecordList(List<WeightTable> items)
        {
            List<WeightTable> results = new List<WeightTable>();

            results.Add(items[0]);

            foreach( var item in items)
            {
                bool isFound = false;
                foreach( var w in results)
                {
                    if ( w.IsInSameTables(item))
                    {
                        isFound = true;
                        break;
                    }
                }

                if(isFound)
                {
                    continue;
                }
                else
                {
                    results.Add(item);
                }
            }

            return results;
        }


    }
}
