﻿using Microsoft.EntityFrameworkCore;
using GamePreView.Application.Interface;
using GamePreView.Domain.Entity;
using GamePreView.Infrastructure.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Threading;

namespace GamePreView.Infrastructure.Repository.Interface
{
    public class RepositoryBase<T> : IAsyncRepository<T> where T : EntityBase
    {
        protected readonly ApplicationDbContext _dbContext;

        public RepositoryBase(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        public async Task<IReadOnlyList<T>> GetAllAsync(CancellationToken cancelToken = default)
        {
            return await _dbContext.Set<T>().ToListAsync(cancelToken);
        }
        public async Task<IReadOnlyList<T>> GetAsync(Expression<Func<T, bool>> predicate, CancellationToken cancelToken = default)
        {
            return await _dbContext.Set<T>().Where(predicate).ToListAsync(cancelToken);
        }
        public async Task<IReadOnlyList<T>> GetAsync(Expression<Func<T, bool>> predicate = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, string includeString = null, bool disableTracking = true, CancellationToken cancelToken = default)
        {
            IQueryable<T> query = _dbContext.Set<T>();
            if (disableTracking) query = query.AsNoTracking();

            if (!string.IsNullOrWhiteSpace(includeString)) query = query.Include(includeString);

            if (predicate != null) query = query.Where(predicate);

            if (orderBy != null)
                return await orderBy(query).ToListAsync(cancelToken);
            return await query.ToListAsync(cancelToken);
        }

        public async Task<IReadOnlyList<T>> GetAsync(Expression<Func<T, bool>> predicate = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, List<Expression<Func<T, object>>> includes = null, bool disableTracking = true, CancellationToken cancelToken = default)
        {
            IQueryable<T> query = _dbContext.Set<T>();
            if (disableTracking) query = query.AsNoTracking();

            if (includes != null) query = includes.Aggregate(query, (current, include) => current.Include(include));

            if (predicate != null) query = query.Where(predicate);

            if (orderBy != null)
                return await orderBy(query).ToListAsync(cancelToken);
            return await query.ToListAsync(cancelToken);
        }

        public virtual async Task<T> GetByKeyAsync(params object[] keyValues)
        {
            return await _dbContext.Set<T>().FindAsync(keyValues);
        }

        public async Task<T> AddAsync(T entity, CancellationToken cancelToken = default)
        {
            _dbContext.Set<T>().Add(entity);
            await _dbContext.SaveChangesAsync(cancelToken);
            return entity;
        }

        public async Task UpdateAsync(T entity, CancellationToken cancelToken = default)
        {
            _dbContext.Entry(entity).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync(cancelToken);
        }

        public async Task DeleteAsync(T entity, CancellationToken cancelToken = default)
        {
            _dbContext.Set<T>().Remove(entity);
            await _dbContext.SaveChangesAsync(cancelToken);
        }

        public async Task<long> GetRecordsNumAsync(CancellationToken cancelToken = default)
        {
            long numbers = await _dbContext.Set<T>().CountAsync(cancelToken);

            return numbers;
        }
    }
}
