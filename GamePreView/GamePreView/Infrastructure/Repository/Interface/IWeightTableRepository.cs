﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GamePreView.Domain.Entity;
using GamePreView.Application;
using GamePreView.Application.Interface;
using GamePreView.Domain.EntityKeys;
using System.Threading;

namespace GamePreView.Infrastructure.Repository.Interface
{
#nullable enable
    public interface IWeightTableRepository : IAsyncRepository<WeightTable>
    {
        Task<IEnumerable<WeightTable>> GetWeightTablesByNameAsync(int gameId, string name, CancellationToken token);

        Task<IEnumerable<WeightTable>> GetWeightTablesByGameIdAsync(int gamdId, CancellationToken token);

        Task<IOrderedEnumerable<WeightTable>> GetWeightTableRecordsAsync(int gameId, int stateId, string name, string var, int bet, int line, int denom, CancellationToken token);

        Task<WeightTable?> GetAWeightTableRecordAsync(WeightTableKeys keys, CancellationToken token);

        Task<IEnumerable<WeightTable>> GetWeightTablesBySelection(WeightTableKeys keys, CancellationToken token);

        Task<ValueTuple<bool, WeightTable?>> CreateWeightTableRecordAsync(WeightTable newItem, CancellationToken token);

        Task<ValueTuple<bool, WeightTable?>> UpdateWeightTableRecordAsync(WeightTable Item, CancellationToken token);

        Task<ValueTuple<bool, WeightTable?>> DeleteAWeightTableRecordAsync(WeightTable Item, CancellationToken token);

        Task<ValueTuple<bool, WeightTable?>> CreateMultiWeightTableRecordsAsync(List<WeightTable> newItem, CancellationToken token);

        Task<ValueTuple<bool, WeightTable?>> UpdateMultiWeightTableRecordsAsync(List<WeightTable> Items, CancellationToken token);

        Task<ValueTuple<bool, WeightTable?>> DeleteMultiWeightTableRecordsAsync(List<WeightTable> Items, CancellationToken token);

        Task<long> GetWeightTableRecordsNumerAsync(CancellationToken token);

    }
}
    