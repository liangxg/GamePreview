﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GamePreView.Domain;
using GamePreView.Application;

namespace GamePreView.Infrastructure
{
    public class PersonRepoistory : IPersonRepoistory
    {
        private IPersonCollections _personCollection;

        public PersonRepoistory(IPersonCollections personCollections)
        {
            this._personCollection = personCollections;
        }


        public  Task<List<Person>> GetAllAsync()
        {
            List<Person> newReturn = new List<Person>();
           /* await Task.Run(() =>
               {
                   newReturn.AddRange(_personCollection.GetPersonList());

               }


            );
           */

            newReturn.AddRange(_personCollection.GetPersonList());

            return Task.FromResult(newReturn);
        }


        public  Task<Person> AddPersonAsync (Person person)
        {
            /* await Task.Run(() =>
                {
                    _personCollection.AddPerson(person);

                }
             ); 
            */
            _personCollection.AddPerson(person);
            return Task.FromResult(person);
        }

        public Task<Person> GetPersonAsync(int id)
        {
            Person person;          
             
            person = _personCollection.GetPerson(id);
             

            return Task.FromResult(person);


        }
    }
}
