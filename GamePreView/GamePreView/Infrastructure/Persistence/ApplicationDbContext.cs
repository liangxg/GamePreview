﻿using Microsoft.AspNetCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.Extensions.Options;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.ApiAuthorization.IdentityServer;
using GamePreView.Domain.Entity;
using GamePreView.Infrastructure.Persistence.Configurations;

namespace GamePreView.Infrastructure.Persistence
{
    public class ApplicationDbContext : DbContext
    {

        public ApplicationDbContext(
            DbContextOptions<ApplicationDbContext> options) : base(options)
        {
            
        }
        public DbSet<CurrentWork> CurrentWorks { get; set; }

        public DbSet<Game> Games { get; set; }

        public DbSet<GamePerformance> Performances { get; set; }

        public DbSet<GameState> GameStates { get; set; }
        public DbSet<MixedPaytable> MixedPaytables { get; set; }
        public DbSet<Paytable> Paytables { get; set; }
        public DbSet<Probability> Probabilities { get; set; }
        public DbSet<ReelStrip> ReelStrips { get; set; }
        public DbSet<Reference> References { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserActivity> UserActivities { get; set; }
        public DbSet<UserGame> UserGames { get; set; }
        public DbSet<UserSession> UserSessions { get; set; }
        public DbSet<WeightTable> Weights { get; set; }


        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            var result = await base.SaveChangesAsync(cancellationToken);

            return result;
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            string _connectString = "server=localhost; database=gamepreview; user=agtadmin; password=Game2022;";
            optionsBuilder.UseSqlServer(_connectString);
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfigurationsFromAssembly(typeof(User).Assembly);
            builder.ApplyConfigurationsFromAssembly(typeof(CurrentWork).Assembly);
            builder.ApplyConfigurationsFromAssembly(typeof(GamePerformance).Assembly);
            builder.ApplyConfigurationsFromAssembly(typeof(GameState).Assembly);
            builder.ApplyConfigurationsFromAssembly(typeof(Probability).Assembly);
            builder.ApplyConfigurationsFromAssembly(typeof(ReelStrip).Assembly);
            builder.ApplyConfigurationsFromAssembly(typeof(UserGame).Assembly);
            builder.ApplyConfigurationsFromAssembly(typeof(WeightTable).Assembly);
            builder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());

            base.OnModelCreating(builder);
        }
    }
}
