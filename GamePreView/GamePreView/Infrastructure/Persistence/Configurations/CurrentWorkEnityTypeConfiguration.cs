﻿using Microsoft.AspNetCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.ApiAuthorization.IdentityServer;
using GamePreView.Domain.Entity;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HelloWoGamePreViewrld.Infrastructure.Persistence.Configurations
{
    public class CurrentWorkEnityTypeConfiguration : IEntityTypeConfiguration<CurrentWork>
    {
        public void Configure(EntityTypeBuilder<CurrentWork> builder)
        {
            builder.HasKey(c => new {c.UserId, c.GameId, c.SessionId});
        }
    }
}
