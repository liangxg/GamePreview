﻿using Microsoft.AspNetCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.ApiAuthorization.IdentityServer;
using GamePreView.Domain.Entity;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GamePreView.Infrastructure.Persistence.Configurations
{
    public class WeightTableEntityTypeConfiguration : IEntityTypeConfiguration<WeightTable>
    {
        public void Configure(EntityTypeBuilder<WeightTable> builder)
        {
            builder.Property(w => w.EntityId).ValueGeneratedOnAdd();
            builder.Property(w => w.CreatedDate).ValueGeneratedOnAdd();
            builder.Property(w => w.LastModifiedDate).ValueGeneratedOnAddOrUpdate();

            builder.Property(w => w.GameId).HasColumnOrder(0);
            builder.Property(w => w.StateId).HasColumnOrder(1);
            builder.Property(w => w.WeightTableName).HasColumnOrder(2);
            builder.Property(w => w.Variation).HasColumnOrder(3);
            builder.Property(w => w.Bet).HasColumnOrder(4);
            builder.Property(w => w.Line).HasColumnOrder(5);
            builder.Property(w => w.Denom).HasColumnOrder(6);
            builder.Property(w => w.ActionValue).HasColumnOrder(7);
            builder.Property(w => w.ActionWeight).HasColumnOrder(8);           

            builder.HasKey(w => new { w.GameId, w.StateId, w.WeightTableName, w.Variation, w.Bet, w.Line, w.Denom, w.ActionValue });
            
        }
    }
}

