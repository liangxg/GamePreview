﻿using Microsoft.AspNetCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.ApiAuthorization.IdentityServer;
using GamePreView.Domain.Entity;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GamePreView.Infrastructure.Persistence.Configurations
{
    public class ReelStripEntityTypeConfiguration : IEntityTypeConfiguration<ReelStrip>
    {
        public void Configure(EntityTypeBuilder<ReelStrip> builder)
        {
            builder.HasKey(r => new { r.GameId, r.StateId, r.ReelNo, r.SetNo });
        }
    }
}
