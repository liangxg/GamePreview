﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GamePreView.Infrastructure.Persistence.Migrations
{
    public partial class CreateDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Games",
                columns: table => new
                {
                    GameId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    GameName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    BrandId = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Status = table.Column<int>(type: "int", nullable: false),
                    Designer = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Jurisdiction = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreateTs = table.Column<DateTime>(type: "datetime2", nullable: false),
                    SubmitTs = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TotalRTP = table.Column<double>(type: "float", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Games", x => x.GameId);
                });

            migrationBuilder.CreateTable(
                name: "References",
                columns: table => new
                {
                    ReferenceId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TableName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FieldName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FieldValue = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_References", x => x.ReferenceId);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    UserId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    LastName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Password = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Role = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    PhoneNo = table.Column<int>(type: "int", nullable: false),
                    MobileNo = table.Column<int>(type: "int", nullable: false),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Status = table.Column<int>(type: "int", nullable: false),
                    UpdateTs = table.Column<DateTime>(type: "datetime2", nullable: false),
                    SessionID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.UserId);
                });

            migrationBuilder.CreateTable(
                name: "GameStates",
                columns: table => new
                {
                    GameId = table.Column<int>(type: "int", nullable: false),
                    StateId = table.Column<int>(type: "int", nullable: false),
                    StateName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    PreStateId = table.Column<int>(type: "int", nullable: false),
                    PreStateName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    TheoriticRTP = table.Column<double>(type: "float", nullable: false),
                    TriggerSymbol = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    TriggerKind = table.Column<int>(type: "int", nullable: false),
                    FreeSpinNo = table.Column<int>(type: "int", nullable: false),
                    IsRetrigger = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    RetriggerSymbol = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    RetriggerKind = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    RetriggerSpin = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GameStates", x => new { x.GameId, x.StateId });
                    table.ForeignKey(
                        name: "FK_GameStates_Games_GameId",
                        column: x => x.GameId,
                        principalTable: "Games",
                        principalColumn: "GameId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Performances",
                columns: table => new
                {
                    PerformanceId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    GameId = table.Column<int>(type: "int", nullable: false),
                    GameName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    BrandId = table.Column<int>(type: "int", nullable: false),
                    Status = table.Column<int>(type: "int", nullable: false),
                    Jurisdiction = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreateTs = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TotalRTP = table.Column<double>(type: "float", nullable: false),
                    ADT = table.Column<int>(type: "int", nullable: false),
                    AvgToRatio = table.Column<double>(type: "float", nullable: false),
                    NumOfRecord = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Performances", x => x.PerformanceId);
                    table.ForeignKey(
                        name: "FK_Performances_Games_GameId",
                        column: x => x.GameId,
                        principalTable: "Games",
                        principalColumn: "GameId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CurrentWorks",
                columns: table => new
                {
                    UserId = table.Column<int>(type: "int", nullable: false),
                    GameId = table.Column<int>(type: "int", nullable: false),
                    SessionId = table.Column<int>(type: "int", nullable: false),
                    Status = table.Column<int>(type: "int", nullable: false),
                    CreateTs = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CurrentWorks", x => new { x.UserId, x.GameId, x.SessionId });
                    table.ForeignKey(
                        name: "FK_CurrentWorks_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserActivities",
                columns: table => new
                {
                    UserActivityId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    Action = table.Column<int>(type: "int", nullable: false),
                    ActionTs = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TableName = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserActivities", x => x.UserActivityId);
                    table.ForeignKey(
                        name: "FK_UserActivities_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserGames",
                columns: table => new
                {
                    UserId = table.Column<int>(type: "int", nullable: false),
                    GameId = table.Column<int>(type: "int", nullable: false),
                    Status = table.Column<int>(type: "int", nullable: false),
                    Role = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreateTs = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdateTs = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserGames", x => new { x.GameId, x.UserId });
                    table.ForeignKey(
                        name: "FK_UserGames_Games_GameId",
                        column: x => x.GameId,
                        principalTable: "Games",
                        principalColumn: "GameId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserGames_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserSessions",
                columns: table => new
                {
                    UserSessionId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    HashKey = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    HashValue = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Status = table.Column<int>(type: "int", nullable: false),
                    SessionStart = table.Column<DateTime>(type: "datetime2", nullable: false),
                    SessionEnd = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Memo = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserSessions", x => x.UserSessionId);
                    table.ForeignKey(
                        name: "FK_UserSessions_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MixedPaytables",
                columns: table => new
                {
                    MixedPaytableId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    GameId = table.Column<int>(type: "int", nullable: false),
                    StateId = table.Column<int>(type: "int", nullable: false),
                    CreateUserId = table.Column<int>(type: "int", nullable: false),
                    UpdateUserId = table.Column<int>(type: "int", nullable: false),
                    SymbolId = table.Column<int>(type: "int", nullable: false),
                    SymbolName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Bet = table.Column<int>(type: "int", nullable: false),
                    Line = table.Column<int>(type: "int", nullable: false),
                    Denom = table.Column<int>(type: "int", nullable: false),
                    Variation = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ReelStrips = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    K_1 = table.Column<int>(type: "int", nullable: false),
                    K_2 = table.Column<int>(type: "int", nullable: false),
                    K_3 = table.Column<int>(type: "int", nullable: false),
                    K_4 = table.Column<int>(type: "int", nullable: false),
                    K_5 = table.Column<int>(type: "int", nullable: false),
                    K_6 = table.Column<int>(type: "int", nullable: false),
                    K_7 = table.Column<int>(type: "int", nullable: false),
                    K_8 = table.Column<int>(type: "int", nullable: false),
                    Memo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    GameStateGameId = table.Column<int>(type: "int", nullable: true),
                    GameStateStateId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MixedPaytables", x => x.MixedPaytableId);
                    table.ForeignKey(
                        name: "FK_MixedPaytables_GameStates_GameStateGameId_GameStateStateId",
                        columns: x => new { x.GameStateGameId, x.GameStateStateId },
                        principalTable: "GameStates",
                        principalColumns: new[] { "GameId", "StateId" });
                });

            migrationBuilder.CreateTable(
                name: "Paytables",
                columns: table => new
                {
                    PaytableId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    GameId = table.Column<int>(type: "int", nullable: false),
                    StateId = table.Column<int>(type: "int", nullable: false),
                    CreateUserId = table.Column<int>(type: "int", nullable: false),
                    UpdateUserId = table.Column<int>(type: "int", nullable: false),
                    SymbolId = table.Column<int>(type: "int", nullable: false),
                    SymbolName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SymbolFullName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SymbolDescription = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Bet = table.Column<int>(type: "int", nullable: false),
                    Line = table.Column<int>(type: "int", nullable: false),
                    Denom = table.Column<int>(type: "int", nullable: false),
                    Variation = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    K_1 = table.Column<int>(type: "int", nullable: false),
                    K_2 = table.Column<int>(type: "int", nullable: false),
                    K_3 = table.Column<int>(type: "int", nullable: false),
                    K_4 = table.Column<int>(type: "int", nullable: false),
                    K_5 = table.Column<int>(type: "int", nullable: false),
                    K_6 = table.Column<int>(type: "int", nullable: false),
                    K_7 = table.Column<int>(type: "int", nullable: false),
                    K_8 = table.Column<int>(type: "int", nullable: false),
                    IsWild = table.Column<int>(type: "int", nullable: false),
                    IsScatter = table.Column<int>(type: "int", nullable: false),
                    IsMixed = table.Column<int>(type: "int", nullable: false),
                    Memo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    GameStateGameId = table.Column<int>(type: "int", nullable: true),
                    GameStateStateId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Paytables", x => x.PaytableId);
                    table.ForeignKey(
                        name: "FK_Paytables_GameStates_GameStateGameId_GameStateStateId",
                        columns: x => new { x.GameStateGameId, x.GameStateStateId },
                        principalTable: "GameStates",
                        principalColumns: new[] { "GameId", "StateId" });
                });

            migrationBuilder.CreateTable(
                name: "Probabilities",
                columns: table => new
                {
                    GameId = table.Column<int>(type: "int", nullable: false),
                    StateId = table.Column<int>(type: "int", nullable: false),
                    ProbabilityName = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    CreateUserId = table.Column<int>(type: "int", nullable: false),
                    UpdateUserId = table.Column<int>(type: "int", nullable: false),
                    Bet = table.Column<int>(type: "int", nullable: false),
                    Line = table.Column<int>(type: "int", nullable: false),
                    Denom = table.Column<int>(type: "int", nullable: false),
                    Variation = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Total = table.Column<double>(type: "float", nullable: false),
                    Shot = table.Column<double>(type: "float", nullable: false),
                    ReferenceId = table.Column<int>(type: "int", nullable: false),
                    Memo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    GameStateGameId = table.Column<int>(type: "int", nullable: true),
                    GameStateStateId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Probabilities", x => new { x.GameId, x.StateId, x.ProbabilityName });
                    table.ForeignKey(
                        name: "FK_Probabilities_GameStates_GameStateGameId_GameStateStateId",
                        columns: x => new { x.GameStateGameId, x.GameStateStateId },
                        principalTable: "GameStates",
                        principalColumns: new[] { "GameId", "StateId" });
                });

            migrationBuilder.CreateTable(
                name: "ReelStrips",
                columns: table => new
                {
                    GameId = table.Column<int>(type: "int", nullable: false),
                    StateId = table.Column<int>(type: "int", nullable: false),
                    ReelNo = table.Column<int>(type: "int", nullable: false),
                    SetNo = table.Column<int>(type: "int", nullable: false),
                    CreateUserId = table.Column<int>(type: "int", nullable: false),
                    UpdateUserId = table.Column<int>(type: "int", nullable: false),
                    Bet = table.Column<int>(type: "int", nullable: false),
                    Line = table.Column<int>(type: "int", nullable: false),
                    Denom = table.Column<int>(type: "int", nullable: false),
                    Variation = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ReelStripValue = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Memo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    GameStateGameId = table.Column<int>(type: "int", nullable: true),
                    GameStateStateId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReelStrips", x => new { x.GameId, x.StateId, x.ReelNo, x.SetNo });
                    table.ForeignKey(
                        name: "FK_ReelStrips_GameStates_GameStateGameId_GameStateStateId",
                        columns: x => new { x.GameStateGameId, x.GameStateStateId },
                        principalTable: "GameStates",
                        principalColumns: new[] { "GameId", "StateId" });
                });

            migrationBuilder.CreateTable(
                name: "Weights",
                columns: table => new
                {
                    GameId = table.Column<int>(type: "int", nullable: false),
                    StateId = table.Column<int>(type: "int", nullable: false),
                    WeightTableName = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    CreateUserId = table.Column<int>(type: "int", nullable: false),
                    UpdateUserId = table.Column<int>(type: "int", nullable: false),
                    Bet = table.Column<int>(type: "int", nullable: false),
                    Line = table.Column<int>(type: "int", nullable: false),
                    Denom = table.Column<int>(type: "int", nullable: false),
                    Variation = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ActionValue = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ActionWeight = table.Column<double>(type: "float", nullable: false),
                    AccumulatedWeight = table.Column<double>(type: "float", nullable: false),
                    ReferenceId = table.Column<int>(type: "int", nullable: false),
                    Memo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    GameStateGameId = table.Column<int>(type: "int", nullable: true),
                    GameStateStateId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Weights", x => new { x.GameId, x.StateId, x.WeightTableName });
                    table.ForeignKey(
                        name: "FK_Weights_GameStates_GameStateGameId_GameStateStateId",
                        columns: x => new { x.GameStateGameId, x.GameStateStateId },
                        principalTable: "GameStates",
                        principalColumns: new[] { "GameId", "StateId" });
                });

            migrationBuilder.CreateIndex(
                name: "IX_MixedPaytables_GameStateGameId_GameStateStateId",
                table: "MixedPaytables",
                columns: new[] { "GameStateGameId", "GameStateStateId" });

            migrationBuilder.CreateIndex(
                name: "IX_Paytables_GameStateGameId_GameStateStateId",
                table: "Paytables",
                columns: new[] { "GameStateGameId", "GameStateStateId" });

            migrationBuilder.CreateIndex(
                name: "IX_Performances_GameId",
                table: "Performances",
                column: "GameId");

            migrationBuilder.CreateIndex(
                name: "IX_Probabilities_GameStateGameId_GameStateStateId",
                table: "Probabilities",
                columns: new[] { "GameStateGameId", "GameStateStateId" });

            migrationBuilder.CreateIndex(
                name: "IX_ReelStrips_GameStateGameId_GameStateStateId",
                table: "ReelStrips",
                columns: new[] { "GameStateGameId", "GameStateStateId" });

            migrationBuilder.CreateIndex(
                name: "IX_UserActivities_UserId",
                table: "UserActivities",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserGames_UserId",
                table: "UserGames",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserSessions_UserId",
                table: "UserSessions",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Weights_GameStateGameId_GameStateStateId",
                table: "Weights",
                columns: new[] { "GameStateGameId", "GameStateStateId" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CurrentWorks");

            migrationBuilder.DropTable(
                name: "MixedPaytables");

            migrationBuilder.DropTable(
                name: "Paytables");

            migrationBuilder.DropTable(
                name: "Performances");

            migrationBuilder.DropTable(
                name: "Probabilities");

            migrationBuilder.DropTable(
                name: "ReelStrips");

            migrationBuilder.DropTable(
                name: "References");

            migrationBuilder.DropTable(
                name: "UserActivities");

            migrationBuilder.DropTable(
                name: "UserGames");

            migrationBuilder.DropTable(
                name: "UserSessions");

            migrationBuilder.DropTable(
                name: "Weights");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "GameStates");

            migrationBuilder.DropTable(
                name: "Games");
        }
    }
}
