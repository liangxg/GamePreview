﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using GamePreView.Application.Queries;
using GamePreView.Application.CreatePerson;
using GamePreView.Application.Queries.GetPerson;
using GamePreView.Application.Queries.GetPersonList;



namespace GamePreView.API.Controllers
{
    
    public class HelloController : ControllerBase
    {
        private readonly ILogger<HelloController> _logger;

        private readonly IMediator _mediator;
        public HelloController(IMediator mediator, ILogger<HelloController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [HttpPost]
        [Route("[controller]/AddPerson")]
        public async Task<IActionResult> AddPerson([FromBody] CreateCommand command)
        {
            var result = await _mediator.Send(command);
            return Ok("hello World!");
        }

        [HttpGet]
        [Route("[controller]/GetPerson/{id}")]
        public async Task<IActionResult> GetPerson(int id)
        {
            var query = new GetPersonQuery(id);
            var person_ = await _mediator.Send(query);
            return Ok(person_);
        }

        [HttpGet]
        [Route("[controller]/GetAllPerson")]
        public async Task<IActionResult> GetAllPerson(int id)
        {
            var query = new GetPersonListQuery();
            var personList_ = await _mediator.Send(query);
            return Ok(personList_);
        }


    }
}
