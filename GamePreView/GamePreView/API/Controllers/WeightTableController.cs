﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using GamePreView.Application.Queries;
using GamePreView.Application.WeightTableMapping.Command;
using GamePreView.Application.WeightTableMapping.Queries;
using GamePreView.Domain.EntityKeys;


namespace GamePreView.API.Controllers
{

    [ApiController]
    [Route("[controller]")]
    public class WeightTableController : ControllerBase
    {
        private readonly ILogger<WeightTableController> _logger;
        private readonly IMediator _mediator;

        public WeightTableController(IMediator mediator, ILogger<WeightTableController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

       // [HttpPost("AddWeightTable")]
        [HttpPost]
        [Route("AddWeightTable")]
        public async Task<IActionResult> AddWeightTableAsync([FromBody] CreateWeightTableCommand command)
        {
            var result = await _mediator.Send(command);

            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error creating new record");
            }
        }

        [HttpGet]
        [Route("GetAWeighTableRecord")]
        public async Task<IActionResult> GetAWeighTableRecordAsync([FromQuery] WeightTableKeys request)
        {
            if (request == null)
            {
                return BadRequest();
            }

            var query = new GetAWeightTableRecordQuery(request);

            var response = await _mediator.Send(query);

            return Ok(response);

        }
    }
}
