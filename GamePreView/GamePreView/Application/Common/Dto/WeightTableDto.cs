﻿using GamePreView.Application.Mapping;
using GamePreView.Domain.Entity;

namespace GamePreView.Application.Common
{
    public class WeightTableDto : IMapFrom<WeightTable>
    {
        
        public int GameId { get; set; }
       
        public int StateId { get; set; }
        
        public string WeightTableName { get; set; }
        public int CreateUserId { get; set; }
        public int UpdateUserId { get; set; }
        public int Bet { get; set; }
        public int Line { get; set; }
        public int Denom { get; set; }
        public string Variation { get; set; }
        
        public string ActionValue { get; set; }
       
        public double ActionWeight { get; set; }
       // public double AccumulatedWeight { get; set; }
        public int ReferenceId { get; set; }
        public string Memo { get; set; }

        public GameState GameState { get; set; }
       
    }
}
