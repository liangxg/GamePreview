﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GamePreView.Domain;

namespace GamePreView.Application
{
    public class PersonCollection : IPersonCollections
    {
        private List<Person> _personList;

        public List<Person> PersonList
        {
            get => _personList;
            set => _personList = value;
        }


        public PersonCollection()
        {
            PersonList = new List<Person>();
            Person p1 = new Person { Name = "Peter", Age = 30, Id = 1 };
            Person p2 = new Person { Name = "John",  Age = 23, Id = 2 };
            Person p3 = new Person { Name = "Alice", Age = 40, Id = 3 };

            PersonList.Add(p1);
            PersonList.Add(p2);
            PersonList.Add(p3);

        }


        public List<Person> GetPersonList()
        {
            return PersonList;
        }

        public Person AddPerson(Person person)
        {
            PersonList.Add(person);

            return person;
        }


        public Person GetPerson(int id)
        {
            Person result = PersonList.FirstOrDefault(x => x.Id == id);

            return result;
        }


    }

     

    
}
