﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;

namespace GamePreView.Application.CreatePerson
{
    public class CreateCommandValidator :AbstractValidator<CreateCommand>
    {

        public CreateCommandValidator()
        {
            RuleFor(c => c.Name).NotEmpty().WithMessage("(Name) is required").NotNull().MaximumLength(50).WithMessage("(Name) must not exceeds 50 characters");
            RuleFor(c => c.Id).NotEmpty().WithMessage("(Name) is required").GreaterThan(0).WithMessage("(Id) must be larger than 0");
            RuleFor(c => c.Age).NotEmpty().WithMessage("(Agr) is required");
        }
    }
}
