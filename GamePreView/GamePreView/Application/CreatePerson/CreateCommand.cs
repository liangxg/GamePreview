﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;


namespace GamePreView.Application.CreatePerson
{
    public class CreateCommand : IRequest
    {
        public string Name
        {
            get;
            set;
        }

        public uint Age
        { get; set; }

        public int Id
        { get; set; }
    }
}
