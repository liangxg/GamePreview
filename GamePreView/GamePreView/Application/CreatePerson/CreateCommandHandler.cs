﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Extensions.Logging;
using MediatR;
using GamePreView.Infrastructure;
using GamePreView.Domain;
using System.Threading;



namespace GamePreView.Application.CreatePerson
{
    public class CreateCommandHandler : IRequestHandler<CreateCommand>
    {

        private readonly IPersonRepoistory _personRepoistory;
        private readonly IMapper _mapper;        
        private readonly ILogger<CreateCommandHandler> _logger;

        public CreateCommandHandler(IPersonRepoistory personRepoistory, IMapper mapper,  ILogger<CreateCommandHandler> logger)
        {
            _personRepoistory = personRepoistory ?? throw new ArgumentNullException(nameof(personRepoistory));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));         
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<Unit> Handle(CreateCommand request, CancellationToken cancellationToken)
        {
            var personEntity = _mapper.Map<Person>(request);
            var newPerson = await _personRepoistory.AddPersonAsync(personEntity);           

            return Unit.Value;
        }
    }
}
