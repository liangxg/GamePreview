﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;

namespace GamePreView.Application.Queries.GetPerson
{
    public class GetPersonQuery : IRequest<PersonVM>
    {
        public int Id;
        public  GetPersonQuery(int id)
        {
            Id = id;
        }

    }
}
