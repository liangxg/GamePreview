﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using MediatR;
using GamePreView.Infrastructure;

namespace GamePreView.Application.Queries.GetPerson
{
    public class GetPersonQueryHandler : IRequestHandler<GetPersonQuery, PersonVM>
    {
        private readonly IPersonRepoistory _personRepoistory;
        private readonly IMapper _mapper;


        public GetPersonQueryHandler(IPersonRepoistory personRepoistory, IMapper mapper)
        {
            _personRepoistory = personRepoistory;

            _mapper = mapper;
        }


        public async Task<PersonVM> Handle(GetPersonQuery request, CancellationToken cancellationToken)
        {
            var person_ = await _personRepoistory.GetPersonAsync(request.Id);
            return _mapper.Map<PersonVM> (person_);
        }



    }
}
