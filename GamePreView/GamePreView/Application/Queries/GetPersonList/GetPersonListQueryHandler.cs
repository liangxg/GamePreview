﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using MediatR;
using GamePreView.Infrastructure;

namespace GamePreView.Application.Queries.GetPersonList
{
    public class GetPersonQueryHandler : IRequestHandler<GetPersonListQuery, List<PersonVM>>
    {
        private readonly IPersonRepoistory _personRepoistory;
        private readonly IMapper _mapper;


        public GetPersonQueryHandler(IPersonRepoistory personRepoistory, IMapper mapper)
        {
            _personRepoistory = personRepoistory;

            _mapper = mapper;
        }


        public async Task<List<PersonVM>> Handle(GetPersonListQuery request, CancellationToken cancellationToken)
        {
            var personList = await _personRepoistory.GetAllAsync();
            return _mapper.Map<List<PersonVM>>(personList);
        }



    }
}
