﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;

namespace GamePreView.Application.Queries.GetPersonList
{
    public class GetPersonListQuery : IRequest<List<PersonVM>>
    {
        public GetPersonListQuery()
        {

        }
    }
}
