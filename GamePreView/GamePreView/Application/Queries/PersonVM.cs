﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GamePreView.Application.Queries
{
    public class PersonVM
    {       
        public string Name
        {
            get; set;
        }

        public uint Age
        { get; set; }

        public int Id
        { get; set; }
    }
}
