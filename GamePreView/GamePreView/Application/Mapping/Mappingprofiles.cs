﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Reflection;
using AutoMapper;
using GamePreView.Domain;
using GamePreView.Application.CreatePerson;
using GamePreView.Application.Queries;
using GamePreView.Domain.Entity;
using GamePreView.Application.WeightTableMapping.Command;

namespace GamePreView.Application.Mapping
{
    public class Mappingprofiles :Profile
    {
        public Mappingprofiles()
        {
            CreateMap<Person, CreateCommand>().ReverseMap();
            CreateMap<Person, PersonVM>().ReverseMap();

            CreateMap<WeightTable, CreateWeightTableCommand>().ReverseMap();

            ApplyMappingsFromAssembly(Assembly.GetExecutingAssembly());
        }

        private void ApplyMappingsFromAssembly(Assembly assembly)
        {
            var types = assembly.GetExportedTypes()
                .Where(t => t.GetInterfaces().Any(i =>
                    i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IMapFrom<>)))
                .ToList();

            foreach (var type in types)
            {
                var instance = Activator.CreateInstance(type);

                var methodInfo = type.GetMethod("Mapping")
                    ?? type.GetInterface("IMapFrom`1")!.GetMethod("Mapping");

                methodInfo?.Invoke(instance, new object[] { this });

            }
        }
    }
}
