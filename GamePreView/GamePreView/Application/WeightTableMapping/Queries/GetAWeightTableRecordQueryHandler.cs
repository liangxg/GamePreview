﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Extensions.Logging;
using MediatR;
using GamePreView.Infrastructure;
using GamePreView.Domain.Entity;
using System.Threading;
using GamePreView.Infrastructure.Repository.Interface;
using GamePreView.Application.Common;
using GamePreView.Domain.EntityKeys;



namespace GamePreView.Application.WeightTableMapping.Queries
{

    public class GetAWeightTableRecordQueryHandler : IRequestHandler<GetAWeightTableRecordQuery, WeightTableDto>
    {
#nullable enable

        private readonly IWeightTableRepository _weightTableRepository;
        private readonly IMapper _mapper;
        private readonly ILogger<GetAWeightTableRecordQueryHandler> _logger;


        public GetAWeightTableRecordQueryHandler(IWeightTableRepository weightTableRepository, IMapper mapper, ILogger<GetAWeightTableRecordQueryHandler> logger)
        {

            _weightTableRepository = weightTableRepository ?? throw new ArgumentNullException(nameof(weightTableRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<WeightTableDto?> Handle(GetAWeightTableRecordQuery request, CancellationToken cancellationToken)
        {

            var response = await _weightTableRepository.GetAWeightTableRecordAsync(request.Key, cancellationToken);


            if (response == null) return null;

            return _mapper.Map < WeightTableDto > (response);

        }

    }
}
