﻿
using MediatR;
using System.ComponentModel.DataAnnotations;
using GamePreView.Application.Common;
using GamePreView.Domain.EntityKeys;

namespace GamePreView.Application.WeightTableMapping.Queries
{
    public class GetAWeightTableRecordQuery :IRequest<WeightTableDto>
    {
        public WeightTableKeys Key;

        public GetAWeightTableRecordQuery(WeightTableKeys key_)
        {
            Key = key_;
        }
    }
}
