﻿using MediatR;
using System.ComponentModel.DataAnnotations;
using GamePreView.Application.Common;

namespace GamePreView.Application.WeightTableMapping.Command
{
    public class CreateWeightTableCommand : IRequest<WeightTableDto>
    {
        [Required]
        public int GameId { get; set; }
        [Required]
        public int StateId { get; set; }
        [Required]
        public string WeightTableName { get; set; }
        public int CreateUserId { get; set; }
        public int UpdateUserId { get; set; }
        public int Bet { get; set; }
        public int Line { get; set; }
        public int Denom { get; set; }
        public string Variation { get; set; }
        [Required]
        public string ActionValue { get; set; }

        [Required]
        public double ActionWeight { get; set; }
      //  public double AccumulatedWeight { get; set; }
        public int ReferenceId { get; set; }
        public string Memo { get; set; }
    }
}
