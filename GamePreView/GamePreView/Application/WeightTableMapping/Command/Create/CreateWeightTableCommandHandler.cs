﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Extensions.Logging;
using MediatR;
using GamePreView.Infrastructure;
using GamePreView.Domain.Entity;
using System.Threading;
using GamePreView.Infrastructure.Repository.Interface;
using GamePreView.Application.Common;

namespace GamePreView.Application.WeightTableMapping.Command
{
#nullable enable
    public class CreateWeightTableCommandHandler : IRequestHandler<CreateWeightTableCommand, WeightTableDto?>
    {
        private readonly IWeightTableRepository _weightTableRepository;
        private readonly IMapper _mapper;
        private readonly ILogger<CreateWeightTableCommandHandler> _logger;

        public CreateWeightTableCommandHandler( IWeightTableRepository weightTableRepository, IMapper mapper, ILogger<CreateWeightTableCommandHandler> logger)
        {
            _weightTableRepository = weightTableRepository ?? throw new ArgumentNullException(nameof(weightTableRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<WeightTableDto?> Handle(CreateWeightTableCommand request, CancellationToken cancellationToken)
        {
            var weightTableEntity = _mapper.Map<WeightTable>(request);           

            var response = await _weightTableRepository.CreateWeightTableRecordAsync(weightTableEntity, cancellationToken);

            if(!response.Item1)
            {
                return null;
            }
            else
            {
                var weightTabledto = _mapper.Map<WeightTableDto>(response.Item2);
                return weightTabledto;
            }
            
        }
    }
}
