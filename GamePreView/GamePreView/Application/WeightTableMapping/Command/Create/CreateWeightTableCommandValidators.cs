﻿
using FluentValidation;

namespace GamePreView.Application.WeightTableMapping.Command
{
    public class CreateWeightTableCommandValidators : AbstractValidator<CreateWeightTableCommand>
    {
        public CreateWeightTableCommandValidators()
        {
            RuleFor(c => c.GameId).NotEmpty().WithMessage("(GameId) is required").GreaterThanOrEqualTo(0).WithMessage("(GameId) cannot be minus");
            RuleFor(c => c.StateId).NotEmpty().WithMessage("(StateId) is required").GreaterThanOrEqualTo(0).WithMessage("(StateId) cannot be minus");
            RuleFor(c => c.WeightTableName).NotEmpty().WithMessage("(WeightTableName) is required").NotNull().WithMessage("Name cannot be null").MinimumLength(3).MaximumLength(100);
            RuleFor(c => c.Variation).NotEmpty().WithMessage("(Variation) is required").NotNull().WithMessage("(Variation) cannot be null");
            RuleFor(c => c.Bet).NotEmpty().WithMessage("(Bet) is required").GreaterThan(0).WithMessage("(Bet) cannot be minus or zero");
            RuleFor(c => c.Line).NotEmpty().WithMessage("(Line) is required").GreaterThan(0).WithMessage("(Line) cannot be minus or zero");
            RuleFor(c => c.Denom).NotEmpty().WithMessage("(Denom) is required").GreaterThan(0).WithMessage("(Denom) cannot be minus or zero");
            RuleFor(c => c.ActionValue).NotEmpty().WithMessage("(ActionValue) is required").NotNull().WithMessage("(ActionValue)cannot be null");
            RuleFor(c => c.ActionWeight).NotEmpty().WithMessage("(ActionWeight) is required").GreaterThan(0).WithMessage("(ActionWeight) cannot be minus or zero");

        }
    }
}
