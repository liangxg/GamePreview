﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GamePreView.Domain;

namespace GamePreView.Application
{
    public interface IPersonCollections
    {
        List<Person> GetPersonList();
        Person AddPerson(Person person);
        Person GetPerson(int id);
    }
}
