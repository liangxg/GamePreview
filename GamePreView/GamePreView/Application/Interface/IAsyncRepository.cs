﻿using GamePreView.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Threading;

namespace GamePreView.Application.Interface
{
    public interface IAsyncRepository<T> where T : EntityBase
    {
        Task<IReadOnlyList<T>> GetAllAsync(CancellationToken cancelToken = default);
        Task<IReadOnlyList<T>> GetAsync(Expression<Func<T, bool>> predicate, CancellationToken cancelToken);
        Task<IReadOnlyList<T>> GetAsync(Expression<Func<T, bool>> predicate = null,
                                        Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
                                        string includeString = null,
                                        bool disableTracking = true,
                                        CancellationToken cancelToken = default);
        Task<IReadOnlyList<T>> GetAsync(Expression<Func<T, bool>> predicate = null,
                                       Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
                                       List<Expression<Func<T, object>>> includes = null,
                                       bool disableTracking = true,
                                       CancellationToken cancelToken = default);
        Task<T> GetByKeyAsync(params object[] keyValues);
        Task<T> AddAsync(T entity, CancellationToken cancelToken = default);
        Task UpdateAsync(T entity, CancellationToken cancelToken = default);
        Task DeleteAsync(T entity, CancellationToken cancelToken = default);

        Task<long> GetRecordsNumAsync(CancellationToken cancelToken = default);
    }
}